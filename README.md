## Инструкция работы с плагином WPSync Webspark

1. Для плагина WPSync Webspark требуется активный плагин WooCommerce и запущенный WP Cron.
2. При всех описанных выше условиях вам нужно только активировать плагин.

> Так как этот плагин работает с WP Cron, а WP Cron активируется только при посещении вашего сайта. Если ваш сайт не принимает посетителей, WP Cron не будет работать. Если вам нужно убедиться, что задания плагина выполняются независимо от посещений сайта, вам следует рассмотреть возможность использования внешнего cron job.