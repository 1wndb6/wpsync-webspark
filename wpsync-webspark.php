<?php
/*
 * Plugin Name: WPSync Webspark
 * Plugin URI: https://gitlab.com/1wndb6/wpsync-webspark
 * Description:
 * Version: 1.0
 * Author: David Alexanyan
 * Author URI:
 * TextDomain: wpsync-webspark
 */

if (!defined('ABSPATH')) {
    exit;
}

define('WPSW_PATH', plugin_dir_path(__FILE__));

register_activation_hook(WPSW_PATH . 'wpsync-webspark.php', 'activation');
register_deactivation_hook(WPSW_PATH . 'wpsync-webspark.php', 'deactivation');

function activation()
{
    if (!class_exists('WooCommerce')) {
        die('The plugin requires active Woocommerce.');
    }

    if (defined('DISABLE_WP_CRON') && DISABLE_WP_CRON) {
        die('For the plugin to work, enable WP Cron.');
    }

    if (!wp_next_scheduled('wpsync_webspark_hook')) {
        wp_schedule_event(time(), 'hourly', 'wpsync_webspark_hook');
    }
}

function deactivation()
{
    wp_unschedule_event(wp_next_scheduled('wpsync_webspark_hook'), 'wpsync_webspark_hook');
}

add_action('wpsync_webspark_hook', 'main');
function main()
{
    $api = apiRequest();
    $api_error = $api->error;
    $api_data = $api->data;
    if ($api_error || empty($api_data)) {
        return;
    }
    $products = wc_get_products(['limit' => -1]);
    if (!empty($products)) {
        foreach ($products as $product) {
            $data = $product->get_data();
            $new_data = [];
            foreach ($api_data as $k => $item) {
                $item = (array)$item;
                if ($data['sku'] === $item['sku']) {
                    $new_data = $item;
                    unset($api_data[$k]);
                    break;
                }
            }
            if (!empty($new_data)) {
                $name = $new_data['name'];
                $desc = $new_data['description'];
                $price = $new_data['price'];
                $in_stock = $new_data['in_stock'];

                $product->set_name($name);
                $product->set_description($desc);
                $product->set_regular_price($price);
                if ($in_stock) {
                    $product->set_manage_stock(true);
                    $product->set_stock_quantity($in_stock);
                } else {
                    $product->set_manage_stock(false);
                }
                $product->save();
            } else {
                $product->delete(true);
            }
        }
    }
    foreach ($api_data as $new_data) {
        $new_data = (array)$new_data;
        $product = new WC_Product_Simple();
        $sku = $new_data['sku'];
        $name = $new_data['name'];
        $desc = $new_data['description'];
        $price = $new_data['price'];
        $in_stock = $new_data['in_stock'];

        $product->set_sku($sku);
        $product->set_name($name);
        $product->set_description($desc);
        $product->set_regular_price($price);
        if ($in_stock) {
            $product->set_manage_stock(true);
            $product->set_stock_quantity($in_stock);
        } else {
            $product->set_manage_stock(false);
        }
        $product->save();
    }
}

function apiRequest()
{
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL            => 'https://wp.webspark.dev/wp-api/products',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => '',
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => 'GET',
    ]);

    $response = curl_exec($curl);

    curl_close($curl);
    $response = json_decode($response);
    if ($response->error) {
        apiRequest();
    }
    return $response;
}